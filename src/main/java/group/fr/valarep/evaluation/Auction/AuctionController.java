package group.fr.valarep.evaluation.Auction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/auctions")
public class AuctionController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private AuctionRepository auctionRepository;

    @Autowired
    public AuctionController(AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public void save(@RequestBody Auction auction) {

        Date now = new Date();
        if (auction.getStartDate().before(now) && auction.getEndDate().before(auction.getStartDate())) {
            throw new IllegalArgumentException("Start date must be > to End date, both must be > than now");
        }

        auctionRepository.save(auction);
    }


}
